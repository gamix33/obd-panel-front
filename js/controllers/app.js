$(document).ready(function() {
    $('select#first-param').material_select();
    $('select#second-param').material_select();

    var profilesSelect = $("select#profiles");

    session.getProfiles(function(profiles){
        for(var i=0; i < profiles.length; i++){
            var profile = profiles[i];
            var profileElement = $("<option></option>").val(profile.id).text(profile.name);
            profilesSelect.append(profileElement);
        }
        profilesSelect.val(session.getActiveProfile());
        profilesSelect.material_select();
    });

    profilesSelect.change(function(e){
        var profileId = $(e.currentTarget).val();
        session.setActiveProfile(profileId);
        getProfileTrips();
    });

    var tripSelect = $("#trip");
    var getProfileTrips = function(){
        tripSelect.html("");

        if(window.location.pathname.indexOf("stats") > -1){
            tripSelect.append($("<option></option>").val("all").text("[wszystkie trasy]"));
        }

        rest.get(
            "obd-data/" + session.getActiveProfile() + "/trips",
            function(trips){
                for(var i=0; i < trips.length; i++){
                    var trip = trips[i];
                    if(trip != null){
                        var tripElement = $("<option></option>").val(trip).text(trip);
                        tripSelect.append(tripElement);
                    }
                }
                tripSelect.find("option").last().prop("selected", true);
                tripSelect.material_select();
            },
            function(errorMessage){
                Materialize.toast(errorMessage, 3000);
            }
        );
    };
    getProfileTrips();

    //$('.config').perfectScrollbar();
});
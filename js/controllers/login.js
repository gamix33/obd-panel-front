$(document).ready(function() {
    $("#login-btn").click(function(e){
        var data = {
            email : $("input#email").val(),
            password : $("input#password").val()
        }

        session.login(
            data,
            function(){},
            function(){}
        );
    });
});

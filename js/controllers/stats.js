
var stats = {
    canvas: null,
    page: null,
    first: false,
    last: false,
    chart: null,

    init: function(){
        var self = this;
        self.canvas = $("#chart")[0].getContext("2d");

        var tripSelect = $("select#trip");
        var firstParamSelect = $("select#first-param");
        var secondParamSelect = $("select#second-param");

        self.drawChartBtn = $(".draw-chart-btn");
        self.drawChartBtn.click(function(e){
            var tripName = tripSelect.val();

            if(!tripName){
                Materialize.toast("Wybierz trasę", 3000);
                return;
            }

            var firstParam = firstParamSelect.val();
            var secondParam = secondParamSelect.val();

            self.drawChart(firstParam, secondParam, tripName);
        });

        self.nextBtn = $("#next-btn");
        self.nextBtn.click(function(e){
            self.nextPage();
        });

        self.prevBtn = $("#prev-btn");
        self.prevBtn.click(function(e){
            self.prevPage();
        });
    },

    setFirst: function(first){
        if(first){
            this.prevBtn.addClass("hide");
        } else {
            this.prevBtn.removeClass("hide");
        }
        this.first = first;
    },

    setLast: function(last){
        if(last){
            this.nextBtn.addClass("hide");
        } else {
            this.nextBtn.removeClass("hide");
        }
        this.last = last;
    },

    drawPage: function(page){
        this.drawChart(this.firstParam, this.secondParam, this.tripName, page);
    },

    prevPage: function(){
        this.drawPage(this.page-1);
    },

    nextPage: function(){
        this.drawPage(this.page+1);
    },

    drawChart: function(firstParam, secondParam, tripName, page){
        var self = this;
        if(!page) page = 0;

        var trip = tripName != "all" ?  encodeURIComponent(tripName) + "/" : "";

        rest.get(
            "obd-data/" + session.getActiveProfile() + "/chart/" + trip + page,
            function(response){
                self.firstParam = firstParam;
                self.secondParam = secondParam;
                self.tripName = tripName;
                self.page = page;
                self.setFirst(response.first);
                self.setLast(response.last);
                self.renderChart(response.content);
            },
            function(errorMessage){
                Materialize.toast(errorMessage, 3000);
            }
        );
    },

    renderChart: function(points){
        var self = this;
        $(".chart-info").hide();

        var labels = [];
        var firstParamData = [];
        var secondParamData = [];

        var time = null;

        points.forEach(function(point) {
            time = moment(point.time * 1000).format("DD-MM-YYYY HH:mm:ss");

            labels.push(time);
            firstParamData.push(point[self.firstParam]);
            secondParamData.push(point[self.secondParam]);
        });

        if(self.chart){
            self.chart.destroy();
        }

        self.chart = new Chart(self.canvas, {
            type: "line",
            data: {
                "labels": labels,
                "datasets": [
                    {
                        "label": self.firstParam.toUpperCase(),
                        "fill": "false",
                        yAxisID: "y-axis-0",
                        "data": firstParamData,
                        "borderColor" : "#de2f26",
                        "backgroundColor" : "rgba(255,0,0,0.2)",
                        "borderWidth" : 1
                    },
                    {
                        "label": self.secondParam.toUpperCase(),
                        "fill": "false",
                        yAxisID: "y-axis-1",
                        "data": secondParamData,
                        "borderColor" : "#0bde48",
                        "backgroundColor" : "rgba(0,255,0,0.2)",
                        "borderWidth" : 1
                    }
                ]
            },
            options: {
                scales: {
                    yAxes: [{
                        position: "left",
                        "id": "y-axis-0"
                    }, {
                        position: "right",
                        "id": "y-axis-1"
                    }]
                }
            }
        });
    }
}

$(document).ready(function() {
    stats.init();
});
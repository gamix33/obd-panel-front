
var trips = {
    map: null,

    init: function(){
        this.map = new google.maps.Map($("#map")[0], {
            center: {lat: 0, lng: 0},
            zoom: 2
        });
    },

    drawTrip: function(tripName){
        var self = this;
        tripName = encodeURIComponent(tripName);
        rest.get(
            "obd-data/" + session.getActiveProfile() + "/trips/" + tripName,
            function(points){
                self.renderTrip(points);
            },
            function(errorMessage){
                Materialize.toast(errorMessage, 3000);
            }
        );
    },

    renderTrip: function(points){
        var self = this;

        var startPoint = {lat: points[0].latitude, lng: points[0].longitude};
        this.map = new google.maps.Map($("#map")[0], {
            center: startPoint,
            zoom: 15
        });

        var tripPathCoordinates = [];

        var infowindows = [];

        points.forEach(function(point) {
            var coord = {
                lat: parseFloat(point.latitude),
                lng: parseFloat(point.longitude)
            };

            tripPathCoordinates.push(coord);

            var infowindow = new google.maps.InfoWindow({
                content: self.pointInfo(point)
            });
            infowindows.push(infowindow);

            var marker = new google.maps.Marker({
                position: coord,
                map: self.map,
                title: point.tripName
            });

            marker.addListener('click', function() {
                infowindows.forEach(function(iw) {iw.close()});
                infowindow.open(map, marker);
            });
        });

        var tripPath = new google.maps.Polyline({
            path: tripPathCoordinates,
            geodesic: true,
            strokeColor: '#22a9ff',
            strokeOpacity: 1.0,
            strokeWeight: 5
        });

        tripPath.setMap(this.map);
    },

    pointInfo: function(point){
        var text = "";
        text += "<div class='info-text'>";
        if(point.rpm)  text += "OBROTY: " + point.rpm + "RPM<br />";
        if(point.speed)  text += "PRĘDKOŚĆ: " + point.speed + "KM/H<br />";
        if(point.acuVoltage)  text += "NAPIĘCIE AKUMLATORA: " + point.acuVoltage + "V<br />";
        if(point.engineCoolantTemperature)  text += "TEMP. PŁYNU CHŁODNICZEGO: " + point.engineCoolantTemperature + "C<br />";
        if(point.oilTemperature)  text += "TEMP. OLEJU: " + point.oilTemperature + "C<br />";
        if(point.airIntakeTemperature)  text += "TEMP. POWIETRZA DOLOTOWEGO: " + point.airIntakeTemperature + "C<br />";
        if(point.throttlePosition)  text += "POZYCJA PRZEPUSTNICY: " + point.throttlePosition + "%<br />";
        if(point.fuelConsumption)  text += "ZUŻYCIE PALIWA: " + point.fuelConsumption + "L/100KM<br />";
        if(point.fuelLevel)  text += "POZIOM PALIWA: " + point.fuelLevel + "%<br />";
        if(point.time)  text += "CZAS: " + moment(point.time * 1000).format("DD-MM-YYYY HH:mm:ss") + "<br />";
        text += "</div>";

        return text;
    }
}

$(document).ready(function() {
    var tripSelect = $("select#trip");
    var drawTripBtn = $(".draw-trip-btn");

    drawTripBtn.click(function(e){
        var tripName = tripSelect.val();

        if(!tripName){
            Materialize.toast("Wybierz trasę", 3000);
            return;
        }

        trips.drawTrip(tripName);
    });
});

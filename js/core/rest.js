var rest = {

    API_URL: "http://155.133.47.82:9001/",
    DEFAULT_HEADRES: {
        "content-type": "application/json",
        "cache-control": "no-cache"
    },

    headers: function(){
        var headers = this.DEFAULT_HEADRES;

        var authToken = getCookie("token");
        if(authToken){
            headers.Authorization = authToken;
        }

        return headers;
    },

    get: function(url, onSuccess, onError){
        $.ajax({
            type: "GET",
            headers: this.headers(),
            url: this.API_URL + url,
            dataType : "json",
            success: function(response){
                onSuccess(response);
            },
            error: function(response){
                onError(response.responseJSON.message);
            }
        });
    },

    post: function(url, data, onSuccess, onError){
        $.ajax({
            type: "POST",
            headers: this.headers(),
            url: this.API_URL + url,
            dataType : "json",
            data: JSON.stringify(data),
            success: function(response){
                onSuccess(response);
            },
            error: function(response){
                onError(response.responseJSON.message);
            }
        });
    },

    delete: function(url, onSuccess, onError){
        $.ajax({
            type: "DELETE",
            headers: this.headers(),
            url: this.API_URL + url,
            dataType : "json",
            success: function(response){
                onSuccess(response);
            },
            error: function(response){
                onError(response.responseJSON.message);
            }
        });
    }
}

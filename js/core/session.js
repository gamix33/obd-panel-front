var session = {
    token: null,
    loginUrl: "/index.html",
    signedUrl: "/stats.html",

    init: function(){
        this.token = getCookie("token");

        $.ajaxSetup({
            headers: {
                "Content-Type" : "application/json",
                "Authorization": this.token
            }
        });

        if(this.token){
            this.auth();
        } else if(window.location.pathname != this.loginUrl){
            window.location.href = this.loginUrl;
        }
    },

    setData: function(token, email, profile){
        setCookie("token", token, 1);
        setCookie("email", email, 1);
        if(profile){
            setCookie("profile", profile, 1);
        }
    },

    getData: function() {
        return {
            email: getCookie("email"),
            token: getCookie("token"),
            profile: getCookie("profile")
        };
    },

    clearData: function() {
        deleteCookie("token");
        deleteCookie("email");
        deleteCookie("profile");
    },

    login: function(data, onSuccess, onError){
        var self = this;
        rest.post(
            "users/login",
            data,
            function(data){
                self.setData(data.token, data.email, data.profiles[0]);
                onSuccess(data);
                window.location.href = self.signedUrl;
            },
            function(errorMessage){
                Materialize.toast(errorMessage, 3000);
                onError(errorMessage);
            }
        );
    },

    auth: function(){
        var self = this;
        rest.get(
            "users/auth",
            function(data){
                self.setData(data.token, data.email);
            },
            function(errorResponse){
                self.clearData();
                if(window.location.pathname != this.loginUrl){
                    window.location.href = self.loginUrl;
                }
            }
        );
    },

    logout: function(){
        var self = this;
        rest.delete(
            "users/logout",
            function(data){
                self.clearData();
                window.location.href = self.loginUrl;
            },
            function(errorResponse){
                Materialize.toast(errorMessage, 3000);
            }
        );
    },

    getProfiles: function(onSuccess) {
        var self = this;
        rest.get(
            "profiles",
            function(data){
                onSuccess(data);
            },
            function(errorResponse){
                Materialize.toast(errorMessage, 3000);
            }
        );
    },

    setActiveProfile: function(profile){
        setCookie("profile", profile, 48);
    },

    getActiveProfile: function(){
        return getCookie("profile");
    }

};

session.init();

$(document).ready(function() {
    $(".logout-btn").click(function(e){
       session.logout();
    });
});
